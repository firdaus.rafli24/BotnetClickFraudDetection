import imblearn
from imblearn.over_sampling import RandomOverSampler,SMOTE
from imblearn.under_sampling import RandomUnderSampler


class Sampling:
    def __init__(self):
        self.overSamp = SMOTE(sampling_strategy=0.1)
        self.underSamp = RandomUnderSampler(sampling_strategy=0.5)
        
    def fitOverSamp(self,X_train,y_train):
        X_train, y_train = self.overSamp.fit_resample(X_train, y_train)
        return X_train, y_train

    def fitUnderSamp(self,X_train,y_train):
        X_train, y_train = self.underSamp.fit_resample(X_train, y_train)
        return X_train, y_train


    
