from Datapreparation import Datapreparation
from Sampling import Sampling

#import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import numpy as np # linear algebra
import socket
import struct
import matplotlib.pyplot as plt
import re
import pickle

# Generate and plot a synthetic imbalanced classification dataset
from matplotlib import pyplot
from numpy import test, where


#from sklearn.preprocessing import OrdinalEncoder
from sklearn import preprocessing
import category_encoders as ce
from sklearn.metrics import accuracy_score,confusion_matrix,classification_report
#from sklearn.feature_selection import mutual_info_classif 
from sklearn import metrics
from sklearn.tree import plot_tree
from imblearn.pipeline import Pipeline






class Preprocess:

    def __init__(self):
        self._stateDict = {'RPA_SPA': 1, 'FSR_SA': 30, 'FPA_FPAC': 296, 'FSRPA_FSA': 77, 'SPA_SA': 31, 'FSA_SRA': 1181, 'FPA_R': 46, 'SPAC_SPA': 37, 'FPAC_FPA': 2, '_R': 1, 'FPA_FPA': 784, 'FPA_FA': 66, '_FSRPA': 1, 'URFIL': 431, 'FRPA_PA': 5, '_RA': 2, 'SA_S': 2, 'SA_RA': 125, 'FA_FPA': 17, 'FA_RA': 14, 'PA_FPA': 48, 'URHPRO': 380,'SRE_SA':380, 'FSRPA_SRA': 8, 'R_':541, 'DCE': 5, 'SA_R': 1674, 'SA_': 4295, 'RPA_FSPA': 4, 'FA_A': 17, 'FSPA_FSPAC': 7, 'RA_': 2230, 'FSRPA_SA': 255, 'NNS': 47, 'SRPA_FSPAC': 1, 'RPA_FPA': 42, 'FRA_R': 10, 'FSPAC_FSPA': 86, 'RPA_R': 3, 'SRPA_FPA': 5, 'SRA_R': 1, 'PA_SA': 339, 'URO': 6, 'URH': 3593, 'MRQ': 4, 'SR_FSA': 1, 'SPA_RPA': 1, 'URP': 23598, 'RPA_A': 1, 'FRA_': 351, 'FSPA_SRA': 91, 'FSA_FSA': 26138, 'PA_': 149, 'FSRA_FSPA': 798, 'FSPAC_FSA': 11, 'SRPA_SRPA': 176, 'SA_SA': 33, 'FSPAC_SPA': 1, 'SRA_RA': 78, 'RPAC_PA': 1, 'FRPA_R': 1, 'SPA_SPA': 2989, 'PA_RA': 3, 'SPA_SRPA': 4185, 'RA_FA': 8, 'FSPAC_SRPA': 1, 'SPA_FSA': 1, 'FPA_FSRPA': 3, 'SRPA_FSA': 379, 'FPA_FRA': 7, 'S_SRA': 81, 'FSA_SA': 6, 'State': 1, 'SRA_SRA': 38, 'S_SPA': 2, 'FSRPAC_SPA': 7, 'SRPA_FSPA': 35460, 'FPA_SPA': 1, 'FSA_FPA': 3, 'FRPA_RA': 1, 'FSAU_SA': 1, 'FSPA_FSRPA': 10560, 'SA_FSA': 358, 'FA_FRA': 8, 'FSRPA_SPA': 2807, 'FSRPA_FSRA': 32, 'FRA_FPA': 6, 'FSRA_FSRA': 3, 'SPAC_FSRPA': 1, 'FS_': 40, 'FSPA_FSRA': 798, 'FSAU_FSA': 13, 'A_R': 36, 'FSRPAE_FSPA': 1, 'SA_FSRA': 4, 'PA_PAC': 3, 'FSA_FSRA': 279, 'A_A': 68, 'REQ': 892, 'FA_R': 124, 'FSRPA_SRPA': 97, 'FSPAC_FSRA':20, 'FRPA_RPA': 7, 'FSRA_SPA': 8, 'INT': 85813, 'FRPA_FRPA': 6, 'SRA_FSRA': 4, 'SPA_SRA': 808, 'S_RAC': 1, 'SPA_FSPA': 2118, 'FSRAU_FSA': 2,'FRPAC_PA':2,'FRPAC_FPA':2, 'RPA_PA': 171,'SPAEC_SPA': 268, 'A_PA': 47, 'SPA_FSRA': 416, 'FPA_FSA': 2, 'PAC_PA': 5, 'SRPA_SPA': 9646, 'SRPA_FSRA': 13, 'FPA_FRPA': 49, 'SRA_SPA': 10, 'SA_SRA': 838, 'PA_PA': 5979, 'FPA_RPA': 27, 'SR_RA': 10, 'RED': 4579, 'CON': 2190507, 'FSRPA_FSPA':13547, 'FSPA_FPA': 4, 'FAU_R': 2, 'ECO': 2877, 'FRPA_FPA': 72, 'FSAU_SRA': 1, 'FRA_FA': 8, 'FSPA_FSPA': 216341, 'S_FSPA': 19, 'ECR': 3316, 'SPAC_FSPA': 12, 'SR_A': 34, 'SEC_': 5, 'FSAU_FSRA': 3, 'FSRA_FSRPA': 11, 'SR_FSPA': 13, 'A_RPA': 1, 'FRA_PA': 3, 'A_RPE': 1, 'RPA_FRPA': 20, '_SRA': 74, 'SRA_FSPA': 293, 'FPA_': 118, 'FSPAEC_FSPA': 2, '_FA': 1, 'FRA_A': 1, 'FSRPA_FSRPA': 379, 'FSRA_SRA': 14, '_FRPA': 1, 'SPAEC_SRPA': 59, 'FSPA_SPA': 517, 'FSA_FRPA': 1, 'PA_A': 159, 'PA_SRA': 1, 'FPA_RA': 5, 'S_': 68710, 'SA_FSRPA': 4, 'FSA_FSRPA': 1, 'SA_SPA': 4, 'RA_A': 5, '_SRPA': 9, 'S_FRA': 156, 'FA_FRPA': 1, 'PA_R': 72, 'FSRPAC_FSRA': 1, '_PA': 7, 'PA_FA':7,'RA_S': 1, 'SA_FR': 2, 'RA_FPA': 6, 'RPA_': 5, 'SR_FSPA': 2395, 'FSA_FSPA': 230, 'UNK': 2, 'A_RA': 9, 'FRPA_': 6, 'URF': 10, 'FS_SA': 97, 'SPAC_SRPA': 8, 'S_RPA': 32, 'SRPA_SRA': 69, 'SA_RPA': 30, 'PA_FRA': 4, 'FSRA_SA': 49, 'FSRA_FSA': 206, 'PA_SPA': 1, 'SRA_PA': 18, 'FA_': 451, 'S_SA': 6917, 'FSPA_SRPA': 427, 'TXD': 542,'SRA_SA': 1514, 'FSRPAC_FSA': 1, 'FPA_FSPA': 10, 'RA_PA': 3, 'SRA_FSA': 709, 'SRPAC_SRPA': 3, 'FSPAC_FSRPA': 10, 'FA_FSA': 191, 'URNPRO': 2, 'PA_RPA': 81, 'FSPAC_SRA':1, 'SRPA_FSRPA': 3054, 'SRPAC_SRPA': 1, 'FA_FA': 259, 'FSPA_SA': 75, 'SR_SRA': 1, 'FSA_FA': 2, 'SRPA_SA': 406, 'SR_SA': 3119, 'FRPA_FA': 1, 'PA_FRPA': 13, 'S_R': 34, 'FSPAEC_FSPAE': 3, 'S_RA': 61105, 'FSPA_FSA': 5326, '_SA': 20, 'SA_FSPA': 15, 'SRPAC_SPA': 8, 'FPA_PA': 19, 'FSRPAE_FSA': 1, 'S_A': 1, 'RPA_RPA': 3, 'NRS': 6, 'RSP': 115, 'SPA_FSRPA': 1144, 'FSRPAC_FSPA': 139}
        self._protoDict = {'arp': 5, 'unas': 13, 'udp': 1, 'rtcp': 7, 'pim': 3, 'udt': 11, 'esp': 12, 'tcp' : 0, 'rarp': 14, 'ipv6-icmp': 9, 'rtp': 2, 'ipv6': 10, 'ipx/spx': 6, 'icmp': 4, 'igmp' : 8} 
    
    def getStateDict(self):
        return self._stateDict
    
    def getprotoDict(self):
        return self._protoDict

    def convertHexInPort(self,port):
        port = str(port)
        x = re.findall("[a-zA-Z]",port)
        if x:
            port = int(port,16)
        else:
            port = int(port)
        return port
    
    def IptoInteger(self,addr):
        addr = str(addr)
        return struct.unpack("!I", socket.inet_aton(addr))[0]
    
    def IntToIp(self,addr):
        return socket.inet_ntoa(struct.pack("!I", addr))
    
    def ClasstoInt(self,label):
        label = str(label)
        if label == 'No':
            label = 0
        else:
            label = 1
        return int(label)

    
if __name__ == '__main__':

     #Instantiate Object
    preparation = Datapreparation()
    preProcessing = Preprocess()
    sampling = Sampling()
    encoder = ce.OrdinalEncoder(cols=['Dir'])

    preparation.setdTrain("train.csv")
    preparation.setdTest("test.csv")

    # Remove StarTime Column
    preparation.removeStartTime()
    # Split X and Y data
    preparation.splitTrainData()
    preparation.splitTestData()
    X_train, y_train = preparation.getTrainData()
    X_test, y_test = preparation.getTestData()

    # print(X_train.head())

    #Convert Proto and State to Int
    protoDict = preProcessing.getprotoDict()
    stateDict = preProcessing.getStateDict()
    X_train=X_train.replace({"Proto": protoDict,"State": stateDict})
    X_test=X_test.replace({"Proto": protoDict,"State": stateDict})

    #Convert Ip to Int
    X_train['SrcAddr']= X_train['SrcAddr'].apply(preProcessing.IptoInteger)
    X_train['DstAddr'] = X_train['DstAddr'].apply(preProcessing.IptoInteger)

    X_test['SrcAddr']= X_test['SrcAddr'].apply(preProcessing.IptoInteger)
    X_test['DstAddr'] = X_test['DstAddr'].apply(preProcessing.IptoInteger)

    #Convert Hex to Int in Port
    X_train['Sport'] = X_train['Sport'].apply(preProcessing.convertHexInPort)
    X_train['Dport'] = X_train['Dport'].apply(preProcessing.convertHexInPort)

    X_test['Sport'] = X_test['Sport'].apply(preProcessing.convertHexInPort)
    X_test['Dport'] = X_test['Dport'].apply(preProcessing.convertHexInPort)

    #Convert Dir to Integer
    X_train = encoder.fit_transform(X_train)
    X_test = encoder.transform(X_test)

    #Convert Class to Integer
    convFunc = np.vectorize(preProcessing.ClasstoInt)
    y_train = convFunc(y_train)
    y_test = convFunc(y_test)
    

    #SMOTE and Undersampling Training Data
    X_train, y_train = sampling.fitOverSamp(X_train, y_train)
    X_train, y_train = sampling.fitUnderSamp(X_train, y_train)

    #Pickle Train and Test Data
    pickle.dump((X_train, y_train), open('./final_train.pkl', 'wb'))
    pickle.dump((X_test, y_test), open('./final_test.pkl', 'wb'))



    