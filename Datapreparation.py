import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)


class Datapreparation:

    # def __init__(self, trainFile = "train.csv", testFile = "test.csv"):
    #     self.dfTrain = pd.read_csv(trainFile)
    #     self.dfTest = pd.read_csv(testFile)
    
    def setdTrain(self,trainFile):
        self._dfTrain = pd.read_csv(trainFile)
    
    def getdTrain(self):
        return self._dfTrain
    
    def getdTest(self):
        return self._dfTest
    
    def setdTest(self, testFile):
        self._dfTest = pd.read_csv(testFile)

    def splitTrainData(self):
        var_columns = [c for c in self._dfTrain.columns if c not in ['ClickFraud']]
        self._Xtrain = self._dfTrain.loc[:,var_columns]
        self._ytrain = self._dfTrain.loc[:,'ClickFraud']

    def getTrainData(self):
        return self._Xtrain, self._ytrain

    def splitTestData(self):
        var_columns = [c for c in self._dfTest.columns if c not in ['ClickFraud']]
        self._Xtest = self._dfTest.loc[:,var_columns]
        self._ytest = self._dfTest.loc[:,'ClickFraud']

    def getTestData(self):
        return self._Xtest, self._ytest

    def removeStartTime(self):
        self._dfTrain.drop(columns=['StartTime'],axis=1, inplace=True)
        self._dfTest.drop(columns=['StartTime'],axis=1,inplace=True)

# if __name__ == "__main__":
#     dataPrep = Datapreparation()
#     dataPrep.setdTrain("train.csv")
#     dTrain = dataPrep.getdTrain()
#     print(type(dTrain))
    

