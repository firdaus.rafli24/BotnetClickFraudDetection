$(document).ready(function(){
    $("#submitFormPreprocess").unbind('click').bind('click',function(e){
        e.preventDefault();
        // e.stopImmediatePropagation();
        var form_data = new FormData();
        var trainFile = $('#trainFile')[0].files[0];
        var testFile = $('#testFile')[0].files[0];
        form_data.append('train_File',trainFile);
        form_data.append('test_File',testFile);
        $.ajax({
            type : 'POST',
            url : "/preprocess",
            data : form_data,
            contentType: false,
            cache: false,
            processData: false,
        }).done(function(data){
            if (data.status == 'success'){
                alert(data.message)
            }else{
                alert(data.message)
            }
        });
        
    });
    $("#submitFormTrain").unbind('click').bind('click',function(e){
        e.preventDefault();
        // e.stopImmediatePropagation();
        var form_data = new FormData();
        var trainFile = $('#uploadTrainFile')[0].files[0];
        form_data.append('train_File',trainFile);
        $.ajax({
            type : 'POST',
            url : "/train",
            data : form_data,
            contentType: false,
            cache: false,
            processData: false,
        }).done(function(data){
            if (data.status == 'success'){
                alert(data.message)
            }else{
                alert(data.message)
            }
        });
        
    });
    // $("#submitFormClassification").unbind('click').bind('click',function(e){
    //     e.preventDefault();
    //     // e.stopImmediatePropagation();
    //     var form_data = new FormData();
    //     var trainedModel = $('#trainedModel')[0].files[0];
    //     var testFile = $('#testFileClassification')[0].files[0];
    //     form_data.append('model_file',trainedModel);
    //     form_data.append('test_File',testFile);
    //     $.ajax({
    //         type : 'POST',
    //         url : "/classification",
    //         data : form_data,
    //         contentType: false,
    //         cache: false,
    //         processData: false,
    //         success: function(response){
    //             window.location.href = "/classification_result"
    //         }
    //     })
    // });
});


// .done(function(data){
//     if (data.status == 'success'){
//         alert(data.result)
//     }else{
//         alert(data.message)
//     }
// });




