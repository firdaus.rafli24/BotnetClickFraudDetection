from doctest import testfile
import pandas as pd
from bitarray import test
import numpy as np 
from sympy import re
from flask import Flask, send_from_directory, render_template, request, redirect, jsonify
import pickle
from Datapreparation import Datapreparation
from Preprocess import Preprocess
from Sampling import Sampling
from InvertDataTest import InvertDataTest
from Model import Model
from sklearn.metrics import accuracy_score,confusion_matrix,classification_report
import category_encoders as ce
from io import BytesIO
from zipfile import ZipFile
import os
from werkzeug.utils import secure_filename
import json
from collections import OrderedDict




#Open Pickled Data
# pickle_inTest = open("final_test.pkl","rb")
# testData = pickle.load(pickle_inTest)

# UPLOAD_FOLDER
ALLOWED_EXTENSIONS_PREPROCESS = {'csv'}
ALLOWED_EXTENSIONS_TRAIN_CLASSIFICATION = {'pkl'}
DATAPATH = 'D:/Kuliah/Sem_8/TA-2/Program/preprocessed/'
TRAINEDPATH = 'D:/Kuliah/Sem_8/TA-2/Program/trained/'
UPLOAD_FOLDER = 'D:/Kuliah/Sem_8/TA-2/Program/upload/'



app = Flask(__name__)
def allowed_file_preprocessing(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS_PREPROCESS

def allowed_file_train_classification(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS_TRAIN_CLASSIFICATION



@app.route("/")
def index():
   title = 'Click Fraud Detection'
   return render_template("index.html",title=title)

@app.route("/preprocess", methods=["GET","POST"])
def preprocess():
   if request.method == 'GET':
      title = 'Data Preprocessing Page'
      return render_template("preprocess.html", title=title)
   elif request.method == 'POST':
      # Get form data
      trainFile = request.files.get('train_File')
      testFile = request.files.get('test_File')
      # Checking form data if its empty or not
      if trainFile and testFile:
         if allowed_file_preprocessing(trainFile.filename) and allowed_file_preprocessing(testFile.filename):
            # Instantiate Object
            preparation = Datapreparation()
            preProcessing = Preprocess()
            sampling = Sampling()
            encoder = ce.OrdinalEncoder(cols=['Dir'])

            # Set df for train and test
            preparation.setdTrain(trainFile)
            preparation.setdTest(testFile)

            # Remove StarTime Column
            preparation.removeStartTime()

            #Separate X and Y data
            preparation.splitTrainData()
            preparation.splitTestData()
            X_train, y_train = preparation.getTrainData()
            X_test, y_test = preparation.getTestData()    

            #Convert Proto and State to Int
            protoDict = preProcessing.getprotoDict()
            stateDict = preProcessing.getStateDict()
            X_train=X_train.replace({"Proto": protoDict,"State": stateDict})
            X_test=X_test.replace({"Proto": protoDict,"State": stateDict})     

            #Convert Ip to Int
            X_train['SrcAddr']= X_train['SrcAddr'].apply(preProcessing.IptoInteger)
            X_train['DstAddr'] = X_train['DstAddr'].apply(preProcessing.IptoInteger)

            X_test['SrcAddr']= X_test['SrcAddr'].apply(preProcessing.IptoInteger)
            X_test['DstAddr'] = X_test['DstAddr'].apply(preProcessing.IptoInteger)

            #Convert Hex to Int in Port
            X_train['Sport'] = X_train['Sport'].apply(preProcessing.convertHexInPort)
            X_train['Dport'] = X_train['Dport'].apply(preProcessing.convertHexInPort)

            X_test['Sport'] = X_test['Sport'].apply(preProcessing.convertHexInPort)
            X_test['Dport'] = X_test['Dport'].apply(preProcessing.convertHexInPort)

            #Convert Dir to Integer
            X_train = encoder.fit_transform(X_train)
            X_test = encoder.transform(X_test)

            #Convert Class to Integer
            convFunc = np.vectorize(preProcessing.ClasstoInt)
            y_train = convFunc(y_train)
            y_test = convFunc(y_test)
      
            #SMOTE and Undersampling Training Data
            X_train, y_train = sampling.fitOverSamp(X_train, y_train)
            X_train, y_train = sampling.fitUnderSamp(X_train, y_train)

            # pickle.dump((processedData), open(DATAPATH +'PreprocessedData.pkl', 'wb'))
            pickle.dump((X_train, y_train), open(DATAPATH +'final_train.pkl', 'wb'))
            pickle.dump((X_test, y_test), open(DATAPATH +'final_test.pkl', 'wb'))

            # pickle_inTrain = open(DATAPATH+"final_train.pkl","rb")
            # processedTrainData = pickle.load(pickle_inTrain)

            # pickle_inTest = open(DATAPATH+"final_test.pkl","rb")
            # processedTestData = pickle.load(pickle_inTest)
            data = {
               'message' : 'The Preprocessing has been completed',
               'status': 'success',
            }
         else:
            data = {
            'message' : 'File extension is not supported',
            'status': 'failed',
         }
      else:
         data = {
            'message' : 'There is no file uploaded',
            'status': 'failed',
         }
      return jsonify(data) 


@app.route("/train", methods=["GET","POST"])
def train():
   if request.method == 'GET':
      title = 'Model Training Page'
      return render_template("train.html", title=title)
   elif request.method == 'POST':
      trainFile = request.files.get('train_File')
      if trainFile:
         if allowed_file_train_classification(trainFile.filename):

            filename = secure_filename(trainFile.filename)
            trainFile.save(os.path.join(UPLOAD_FOLDER, filename))

            pickle_inTrain = open(UPLOAD_FOLDER + filename,"rb")
            trainData = pickle.load(pickle_inTrain)

            X_train = trainData[0]
            y_train = trainData[1]

            model = Model()
            model.setTrainData(X_train,y_train)
            X_train = model.getXtrain()
            y_train = model.getYtrain()
            model.train(X_train,y_train)

            pickle.dump((model.getModel()), open(TRAINEDPATH+'ClickFraudDetection.pkl', 'wb'))
            data = {
                  'message' : 'The model has been trained',
                  'status': 'success',
               }
         else:
            data = {
               'message' : 'File extensions are not supported',
               'status': 'failed',
            }
      else:
         data = {
               'message' : 'There is no file uploaded',
               'status': 'failed',
            }
      return jsonify(data)


@app.route("/classification", methods=["GET","POST"])
def classification():
   if request.method == 'GET':
      title = 'Do Classification Page'
      return render_template("classification.html",title=title)
   elif request.method == 'POST':
      modelFile = request.files.get('trainedmodel')
      testFile = request.files.get('dataLatih')
      if modelFile and testFile:
         if allowed_file_train_classification(modelFile.filename) and allowed_file_train_classification(testFile.filename):
            
            modelFilename = secure_filename(modelFile.filename)
            modelFile.save(os.path.join(UPLOAD_FOLDER, modelFilename))
            
            testFilename = secure_filename(testFile.filename)
            testFile.save(os.path.join(UPLOAD_FOLDER, testFilename))

            pickle_inModel = open(UPLOAD_FOLDER + modelFilename,"rb")
            trainedModel = pickle.load(pickle_inModel)

            pickle_inTest = open(UPLOAD_FOLDER + testFilename,"rb")
            testData = pickle.load(pickle_inTest)

            X_test = testData[0]
            y_test = testData[1]

            model = Model()
            model.setModel(trainedModel)

            y_pred_en = model.classificaion(X_test)

            X_test["Actual"] = y_test
            X_test["Predicted"] = y_pred_en

            revertData = InvertDataTest()
            revertData.revStateDict()
            revertData.revProtoDict()

            revProtoDict = revertData.getRevProtoDict()
            revStateDict = revertData.getRevStateDict()

            print(revProtoDict)
            print(revStateDict)


            X_test=X_test.replace({"Proto": revProtoDict,"State": revStateDict}) 

            X_test['SrcAddr']= X_test['SrcAddr'].apply(revertData.IntToIp)
            X_test['DstAddr'] = X_test['DstAddr'].apply(revertData.IntToIp)


            X_testNotClickFraud = X_test.query('Actual == 0 & Predicted == 0').head()
            X_testClickFraud = X_test.query('Actual == 1 & Predicted == 1').head()
            X_testFalsePositive = X_test.query('Actual == 0 & Predicted == 1').head()
            X_testFalseNegative = X_test.query('Actual == 1 & Predicted == 0').head()

            X_Final = pd.concat([X_testNotClickFraud,X_testClickFraud,X_testFalsePositive,X_testFalsePositive,X_testFalseNegative],ignore_index=True)
            X_Final.index += 1

            data = {
               'result' : X_test,
               'message' : 'Model accuracy score with gini criterion: {0:0.4f}'. format(accuracy_score(y_test, y_pred_en)),
               'status': 'success',
            }
            return render_template("classification_result.html",tables=[X_Final.to_html(classes='classificationResults')])
         else:
            data = {
               'message' : 'File extensions are not supported',
               'status': 'failed',
            }
      else:
         data = {
               'message' : 'There is no file uploaded',
               'status': 'failed',
            }   
    
if __name__ == "__main__":
    app.run(host="0.0.0.0", port="5000")