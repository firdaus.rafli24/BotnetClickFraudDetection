import pickle
from Preprocess import Preprocess
from Model import Model
from sklearn.metrics import accuracy_score,confusion_matrix,classification_report


if __name__ == '__main__':
    FILEPATH = 'D:/Kuliah/Sem_8/TA-2/Program/preprocessed/'
    #Open Pickled Data
    pickle_inTrain = open(FILEPATH + "final_train.pkl","rb")
    trainData = pickle.load(pickle_inTrain)

    pickle_inTest = open(FILEPATH+"final_test.pkl","rb")
    testData = pickle.load(pickle_inTest)

    # dTreeModel = pickle.load(open("ClickFraudDetection.pkl","rb"))

    #Separate X and Y train
    X_train = trainData[0]
    y_train = trainData[1]

    #Separate X and Y test
    X_test = testData[0]
    y_test = testData[1]
    
    
    #Instantiate Model Class

    model = Model()
    model.setTrainData(X_train,y_train)
    X_train = model.getXtrain()
    y_train = model.getYtrain()
    model.train(X_train,y_train)
    # model.setModel(dTreeModel)
    # dtC = model.getModel()

    y_pred_en = model.classificaion(X_test)
    print(y_pred_en)
    print('Model accuracy score with gini criterion: {0:0.4f}'. format(accuracy_score(y_test, y_pred_en)))

    X_test["Actual"] = y_test
    X_test["Predicted"] = y_pred_en
    print(X_test)

    dtC = model.getModel()
    pickle.dump((dtC), open('./ClickFraudDetection.pkl', 'wb'))

    





    