from sklearn.tree import DecisionTreeClassifier
import pickle


class Model: 

    _Dtc = DecisionTreeClassifier(criterion='gini', random_state=0)

    # def __init__(self,X_train=None, y_train=None):
    #     if  not X_train.empty() and not y_train.empty():
    #         self.X_train = X_train
    #         self.y_train = y_train

    def train(self,X_train,y_train):
        self._model = self._Dtc.fit(X_train, y_train)
    
    def classificaion(self,X_test):
        result = self._model.predict(X_test)
        return result

    def setModel(self,model):
        self._model = model

    def getModel(self):
        return self._model

    def setTrainData(self,X_train,y_train):
         self._X_train = X_train
         self._y_train = y_train
        
    def getXtrain(self):
        return self._X_train
    
    def getYtrain(self):
        return self._y_train
    
